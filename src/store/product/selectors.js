import { createSelector } from "reselect";

const globalSelector = (state) => state.product;

const productCount = createSelector([globalSelector], (global) => global.productCount);

const loader = createSelector([globalSelector], (global) => global.loader);

const products = createSelector([globalSelector], (global) => global.products);

const productIndication = createSelector([globalSelector], (global) => global.productIndication);


const selectors = { productCount, loader, products, productIndication };

export default selectors;