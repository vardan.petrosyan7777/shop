import {useRef} from "react";
import Portal from "../../portal";
import {Popper} from "@mui/material";

const Popover = ({onClose,reference,placement,children}) => {
    const popperRef = useRef()

    return  <Portal>
                <Popper innerRef={popperRef}
                        referenceElement={reference}
                        placement={placement}>
                        {(ref,style)=>(
                            <div ref={ref} style={style} className={'popover'}>
                                {children}
                            </div>
                        )}
                </Popper>
            </Portal>
}