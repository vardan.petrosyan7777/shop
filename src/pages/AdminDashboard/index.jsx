import * as React from 'react';
import { connect } from "react-redux";
import Button from '@mui/material/Button';
import CssBaseline from '@mui/material/CssBaseline';
import TextField from '@mui/material/TextField';
import Link from '@mui/material/Link';
import Box from '@mui/material/Box';
import Typography from '@mui/material/Typography';
import Container from '@mui/material/Container';
import Radio from '@mui/material/Radio';
import RadioGroup from '@mui/material/RadioGroup';
import FormControlLabel from '@mui/material/FormControlLabel';
import FormControl from '@mui/material/FormControl';
import FormLabel from '@mui/material/FormLabel';
import { createTheme, ThemeProvider } from '@mui/material/styles';
import { setProductCount } from "../../store/product/productSlice"
import useAuth from "../../hooks/useAuth";
import Avatar from "@mui/material/Avatar";
import Home from "@mui/icons-material/Home";

function Copyright(props) {
    return (
        <Typography variant="body2" color="text.secondary" align="center" {...props}>
            {'Copyright © '}
            <Link color="inherit" href="https://mui.com/">
                Your Website
            </Link>{' '}
            {new Date().getFullYear()}
            {'.'}
        </Typography>
    );
}
function RowRadioButtonsGroup({defaultValue = 'one'}) {
    return (
        <FormControl>
            <FormLabel id="demo-row-radio-buttons-group-label">Product Count</FormLabel>
            <RadioGroup
                row
                aria-labelledby="demo-row-radio-buttons-group-label"
                name="productCount"
                defaultValue={defaultValue}
            >
                <FormControlLabel value="One" control={<Radio />} label="One" />
                <FormControlLabel value="Several" control={<Radio />} label="Several" />
            </RadioGroup>
        </FormControl>
    );
}
const theme = createTheme();

const AdminDashboard = (props) => {
    const { productCount, setProductCount } = props
    const handleSubmit = (event) => {
        event.preventDefault();
        const data = new FormData(event.currentTarget);
        setProductCount(data.get('productCount'))
    };
    return (
        <ThemeProvider theme={theme}>
            <Container component="main" maxWidth="xs">
                <CssBaseline />
                <Box
                    sx={{
                        marginTop: 8,
                        display: 'flex',
                        flexDirection: 'column',
                        alignItems: 'center',
                    }}
                >
                    <Link href={'/'}>
                        <Avatar sx={{ m: 1, bgcolor: 'secondary.main' }}>
                            <Home />
                        </Avatar>
                    </Link>
                    <Box component="form" onSubmit={handleSubmit} noValidate sx={{ mt: 1 }}>

                        <RowRadioButtonsGroup defaultValue={productCount}/>
                        <Button
                            type="submit"
                            fullWidth
                            variant="contained"
                            sx={{ mt: 3, mb: 2 }}
                        >
                            Save
                        </Button>
                    </Box>
                </Box>
                <Copyright sx={{ mt: 8, mb: 4 }} />
            </Container>
        </ThemeProvider>
    );
}
const mapStateTopProps = (state) => ({
    productCount: state.product.productCount,
})
const mapDispatchToProps = {
    setProductCount:setProductCount,
}

export default connect(mapStateTopProps, mapDispatchToProps)(AdminDashboard);