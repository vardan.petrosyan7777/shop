import { createSelector } from "@reduxjs/toolkit";

const userSelector = (state) => state.user;

const userData = createSelector([userSelector], (user) => user.userData);

const userDbData = createSelector([userSelector], (user) => user.userDbData);

const selectors = { userData, userDbData };

export default selectors;