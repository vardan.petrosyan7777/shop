import { productSlice } from "./productSlice";

export { default as productOp } from "./operations";
export { default as productSel } from "./selectors";

export default productSlice;