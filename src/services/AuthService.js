import API, { axios } from "../api/API"
import AuthResponse from "../models/AuthResponse"
import selectors from '../store/user/selectors'

export default class AuthService {
    static async registration(form) {
        try {
            function registratOnstorage (form) {
                return {
                    data:[{
                        accessToken:'12345678',
                        refreshToken:'12345678',
                        user:{
                            id:new Date().getTime(),
                            email: form.get('email'),
                            password: form.get('password'),
                            firstName:form.get('firstName'),
                            lastName:form.get('lastName'),
                            role:'user',
                        }
                    }],
                    accepted: true,
                    errorMessages : []
                }
            }
            // const response = await API.post("/auth/registration", form)
            const response = registratOnstorage(form)
            if (!response.accepted) {
                throw new Error("Can not create the account")
            }
            return new AuthResponse(response.data[0])
        } catch (e) {
            console.log(e);
            return null
        }
    }
    static async login(data,userDbData) {
        try {
            function loginWhithStorage (data,userDbData) {
                const dataReq = {
                    email: data.get('email'),
                    password: data.get('password'),
                }
                const userData = userDbData[0].user
                const result = {
                    data:[],
                    accepted: false,
                    errorMessages : ["Can not login in system"]
                }
                if(userData.email === dataReq.email && userData.password ===  dataReq.password){
                    result.data.push({
                        accessToken:'12345678',
                        refreshToken:'12345678',
                        user:{
                            id:userData.id,
                            email: userData.email,
                            password: userData.password ,
                            firstName: userData.firstName ,
                            lastName: userData.lastName ,
                            role:userData.role
                        }
                    })
                    result.accepted = true
                    result.errorMessages = []
                }
                console.log (result,'result')
                return result
            }
            // const response = await API.post("/auth/login", {
            //     email: data.get('email'),
            //     password: data.get('password'),
            // })
            const response = loginWhithStorage(data,userDbData)
            if (!response.accepted) {
                throw new Error("Can not login in system")
            }
            return new AuthResponse(response.data[0])
        } catch (e) {
            console.log(e);
            return null
        }
    }
    static async logout() {
        try {
            // const response = await API.post("/auth/logout")
            const response = {
                accepted:true
            }
            if (!response.accepted) {
                throw new Error("Something gones wrong during logout")
            }
            return true
        } catch (e) {
            console.log(e);
            return null
        }
    }

    static async refresh() {
        return await axios.get(process.env.REACT_APP_API + '/auth/refresh', { withCredentials: true })
    }
}