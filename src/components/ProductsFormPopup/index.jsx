import * as React from 'react';
import {connect} from "react-redux";
import {setProducts} from "../../store/product/productSlice";
import Stack from "@mui/material/Stack";
import Button from "@mui/material/Button";
import Dialog from "@mui/material/Dialog";
import DialogTitle from "@mui/material/DialogTitle";
import DialogContent from "@mui/material/DialogContent";
import DialogActions from "@mui/material/DialogActions";
import ProductForm from "../ProductForm"
import AddIcon from '@mui/icons-material/Add';


const ProductsFormPopup = (props) => {
    const {handleAddProduct,productCount} = props
    const [open, setOpen] = React.useState(false);
    const [productC, setProductC] = React.useState([1]);

    const handleClickOpen = () => {
        setOpen(true);
    };

    const handleClose = () => {
        setOpen(false);
        setProductC([1])
    };
    const handleSave = (event) =>{
        event.preventDefault();
        const data = new FormData(event.currentTarget);
        const names = data.getAll('name')
        const prices = data.getAll('price')
        const weights = data.getAll('weight')
        const datesStart = data.getAll('dateStart')
        const datesEnd = data.getAll('dateEnd')
        const result = []
        productC.forEach((el,i)=>{
            result.push({
                id:new Date().getTime()+i,
                name:names[i],
                price:prices[i],
                weight:weights[i],
                dateStart:datesStart[i],
                dateEnd:datesEnd[i]
            })
        })

        handleAddProduct(result)
        setProductC([1])
        handleClose()
    }
    const addCustomFile = (count) => {
        setProductC((prev)=>[...prev,prev.length+2])
    }
    return (
        <Stack
            sx={{ pt: 4 }}
            direction="row"
            spacing={2}
            justifyContent="center"
        >
            <Button variant="outlined" onClick={handleClickOpen}>
                Add Product
            </Button>
            <Dialog open={open}
                    onClose={handleClose}
                    component="form"
                    onSubmit={handleSave}>
                <DialogTitle>ADD PRODUCT</DialogTitle>
                <DialogContent>
                    {productC?.map((el)=>(
                        <div style={{'display':'flex'}} key={el}>
                            <ProductForm/>
                        </div>
                    ))}
                    <Button
                        size="small"
                        color="secondary"
                        disabled={productCount === 'One'? true : false}
                        onClick={() => addCustomFile()}
                    >
                        <AddIcon />
                    </Button>
                </DialogContent>
                <DialogActions>
                    <Button onClick={handleClose}>Cancel</Button>
                    <Button type="submit">Save</Button>

                </DialogActions>
            </Dialog>
        </Stack>
    );
}

const mapStateTopProps = (state) => ({
    productCount:state.product.productCount
})
const mapDispatchToProps = {
}

export default connect(mapStateTopProps, mapDispatchToProps)(ProductsFormPopup);