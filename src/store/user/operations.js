import { userSlice } from "./userSlice";

const { setUserData, setUserDbData } = userSlice.actions;

const handleSetUserData = (data) => (dispatch) => {
    dispatch(setUserData(data));
};
const handleSetUserDbData = (data) => (dispatch) => {
    dispatch(setUserDbData(data));
};

const operations = { handleSetUserData, handleSetUserDbData };

export default operations;