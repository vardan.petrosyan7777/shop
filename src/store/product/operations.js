import { productSlice } from "./productSlice";

const { setProductCount, setLoader, setProducts, setProductIndication } =
    productSlice.actions;

const handleProductCount = (state) => (dispatch) => {
    dispatch(setProductCount(state));
};

const handleLoaderState = (state) => (dispatch) => {
    dispatch(setLoader(state));
};

const handleProducts = (state) => (dispatch) => {
    dispatch(setProducts(state));
};
const handleProductIndication = (state) => (dispatch) => {
    dispatch(setProductIndication(state));
};


const operations = {
    handleProductCount,
    handleLoaderState,
    handleProducts,
    handleProductIndication
};

export default operations;