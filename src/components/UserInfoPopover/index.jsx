import * as React from 'react';
import Typography from '@mui/material/Typography';
import Button from '@mui/material/Button';
import Link from '@mui/material/Link';
import Box from '@mui/material/Box';
import Grid from '@mui/material/Grid';
import Popover from '@mui/material/Popover';
import PopupState, { bindTrigger, bindPopover } from 'material-ui-popup-state';
import {setAuth} from "../../store/global/globalSlice";
import {setUserData} from "../../store/user/userSlice";
import {connect} from "react-redux";
import useAuth from "../../hooks/useAuth";

const  UserInfoPopover = (props) => {
    const {userData} = props
    const { logout } = useAuth()

    const handleLogout = () => {
        logout()
    }

    return (
        <PopupState variant="popover" popupId="demo-popup-popover">
            {(popupState) => (
                <div>
                    <Button variant="contained" {...bindTrigger(popupState)}>
                        {userData.email}
                    </Button>
                    <Popover
                        {...bindPopover(popupState)}
                        anchorOrigin={{
                            vertical: 'bottom',
                            horizontal: 'center',
                        }}
                        transformOrigin={{
                            vertical: 'top',
                            horizontal: 'center',
                        }}
                    >
                <Box sx={{ mt: 3 }}>
                    <Grid container justifyContent="center" textAlign="center">
                        <Grid item xs={10} sm={10} justifyContent="center">
                            <Link color="inherit" href="/userProfile"  variant="body2">
                                <Typography gutterBottom variant="p" component="p">
                                    Profile
                                </Typography>
                            </Link>
                            {
                                userData.role === 'admin' && (
                                    <Link color="inherit" href="/admin"  variant="body2">
                                        <Typography gutterBottom variant="p" component="p">
                                            Admin Dashboard
                                        </Typography>
                                    </Link>
                                )
                            }
                            <Button color="inherit" onClick={handleLogout}>
                                <Typography gutterBottom variant="p" component="p">
                                Logout
                                </Typography>
                            </Button>
                        </Grid>
                    </Grid>
                </Box>
                    </Popover>
                </div>
            )}
        </PopupState>
    );
}

const mapStateTopProps = (state) => ({
    isAuth: state.global.isAuth,
    userData:state.user.userData,

})
const mapDispatchToProps = {
    setAuth:setAuth,
    setUser:setUserData
}

export default connect(mapStateTopProps, mapDispatchToProps)(UserInfoPopover);