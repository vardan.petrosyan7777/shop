import React, { useEffect, useState } from 'react';
import AttachFileIcon from '@mui/icons-material/AccessAlarm';
import KeyOffIcon from '@mui/icons-material/KeyOff';
import AlternateEmailIcon from '@mui/icons-material/AlternateEmail';
import ChangeEmail from "../../components/ChangeEmail";
import ChangePassword from "../../components/ChangePassword";
import user from '../../assets/images/user.png'

import './style.css'




function UserPage() {

    const [userImg, setUserImg] = useState(user);
    const [emailPopup, setEmailPopup] = useState(false)
    const [pswPopup, setPswPopup] = useState(false)

    const imageHandler = (e) => {
        const reader = new FileReader();
        reader.onload = () => {
            if (reader.readyState === 2) {
                setUserImg(reader.result)
            }
        }
        reader.readAsDataURL(e.target.files[0])
    }


    const emailHandler = () => {
        setEmailPopup(true)
    }

    const pswHandler = () => {
        setPswPopup(true)
    }




    return (
        <section className='user_container'>
            <div className='user_info'>
                <div className='user_img_container'>
                    <img className="user_img" src={userImg} alt="member" />
                </div>
                <h3>UserName</h3>
                <h3>Email</h3>
            </div>
            <div className='user_score'>
                <h2>Favourite Games</h2>
            </div>
            <div className='user_settings'>
                <h2>Settings</h2>
                <div className='settings'>
                    <label>
                        <input type="file" name='image-upload' accept='image/*' onChange={imageHandler} />
                        <div className='icon'><svg data-testid="AttachFileIcon"></svg> </div>
                        <p>Upload an Image</p>
                    </label>
                    <button onClick={emailHandler} className='icon-btn'>
                        <svg data-testid="AlternateEmailIcon"></svg>
                        <p>Change Email</p>
                    </button>
                    <ChangeEmail triggerEmail={emailPopup} setTriggerEmail={setEmailPopup} />

                    <button onClick={pswHandler} className='icon-btn'>
                        <svg data-testid="KeyOffIcon"></svg>
                        <p>Change Password</p>
                    </button>
                    <ChangePassword triggerPsw={pswPopup} setTriggerPsw={setPswPopup} />
                </div>
            </div>
        </section>
    );
}

export default UserPage;