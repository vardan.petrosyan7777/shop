import "./utils/polyfills"
import React from 'react';
import ReactDOM from 'react-dom/client';
import { Provider } from "react-redux";
import { PersistGate } from "redux-persist/integration/react"
import { BrowserRouter } from "react-router-dom";
import  store, { persistor }  from "./store/reducers";
import App from './App';
import axios from "axios"
import './index.css';

axios.interceptors.request.use(
    async (config) => {
        const token = await localStorage.getItem("accessToken")
        if (token) {
            config.headers["accessToken"] = `Bearer ${token}`
            config.headers["Authorization"] = `Bearer ${token}`
        }
        return config
    },
    (error) => {
        return Promise.reject(error)
    }
)
axios.interceptors.response.use(
    (response) => {
        return response
    },
    (err) => {
        return new Promise(async (resolve, reject) => {
            if (err.response.status === 401) {
                const token = localStorage.getItem("accessToken")
                const refreshToken = localStorage.getItem("refreshToken")
                const headers = {
                    accessToken: token,
                    refreshToken: refreshToken,
                }
                await axios
                    .put(
                        `${process.env.REACT_APP_API}/api/User/RefreshAccessToken`,
                        {
                            headers: headers,
                        }
                    )
                    .then(async (response) => {
                        if (response.data.accepted === true) {
                            await localStorage.setItem(
                                "accessToken",
                                response.data.data[0].accessToken
                            )
                            await localStorage.setItem(
                                "refreshToken",
                                response.data.data[0].refreshToken
                            )
                            err.config.headers[
                                "accessToken"
                                ] = `Bearer ${response.data.data[0].accessToken}`
                            err.config.headers[
                                "Authorization"
                                ] = `Bearer ${response.data.data[0].accessToken}`
                            return axios(err.config)
                        } else {
                            await localStorage.removeItem("accessToken")
                            await localStorage.removeItem("refreshToken")
                            return Promise.reject(err)
                        }
                    })
                return axios(err.config)
            } else {
                return Promise.reject(err)
            }
        })
    }
)
const root = ReactDOM.createRoot(document.getElementById("root"));

root.render(
    <Provider store={store} >
        <PersistGate loading={null} persistor={ persistor }>
            <BrowserRouter>
                <React.StrictMode>
                    <App />
                </React.StrictMode>
            </BrowserRouter>
        </PersistGate>
    </Provider>,
);
