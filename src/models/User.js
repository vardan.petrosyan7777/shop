export default class User {
    email = null
    id = null
    password = null
    firstName = null
    lastName = null
    role = null
    constructor(data) {
        this.email = data.email
        this.id = data.id
        this.password = data.password
        this.firstName = data.firstName
        this.lastName = data.lastName
        this.role = data.role
    }
}