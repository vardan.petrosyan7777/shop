import { createSlice } from "@reduxjs/toolkit";
import { initialProductState } from "./initialState";

export const productSlice = createSlice({
    name: "product",
    initialState: initialProductState,
    reducers: {
        setProductCount(state, action) {
            state.productCount = action.payload;
        },
        setLoader(state, action) {
            state.loader = action.payload;
        },
        setProducts(state, action) {
            state.products = [
                ...action.payload,
                ...state.products
            ];
        },
        setProductIndication(state, action) {
            state.productIndication = action.payload
        },
    },
});
export const  {setProductCount,setLoader,setProducts, setProductIndication } = productSlice.actions

export default productSlice.reducer