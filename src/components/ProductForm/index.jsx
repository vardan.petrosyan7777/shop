import * as React from 'react';
import {connect} from "react-redux";
import Button from "@mui/material/Button";
import TextField from "@mui/material/TextField";

const ProductForm = (props) => {
    return (
        <div style={{'display':'flex'}}>
            <TextField
                autoFocus
                margin="dense"
                id="name"
                name="name"
                label="Product Name"
                type="text"
                fullWidth
                variant="standard"
                required
            />
            <TextField
                autoFocus
                margin="dense"
                id="price"
                name="price"
                label="Price USD"
                type="number"
                fullWidth
                variant="standard"
                required
            />
            <TextField
                autoFocus
                margin="dense"
                id="weight"
                name="weight"
                label="Weight KG"
                type="number"
                fullWidth
                variant="standard"
                required
            />
            <TextField
                autoFocus
                margin="dense"
                id="dateStart"
                name="dateStart"
                label="Start At"
                type="date"
                fullWidth
                variant="standard"
                required
            />
            <TextField
                autoFocus
                margin="dense"
                id="dateEnd"
                name="dateEnd"
                label="End At"
                type="date"
                fullWidth
                variant="standard"
                required
            />

        </div>
    );
}

const mapStateTopProps = (state) => ({
})
const mapDispatchToProps = {
}

export default connect(mapStateTopProps, mapDispatchToProps)(ProductForm);