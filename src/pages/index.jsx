import { lazy, Suspense,useMemo } from "react"
import { Navigate, Routes, Route } from "react-router-dom"
import Loader from '../components/UI/Loader'
import { connect } from "react-redux"
import { globalOp } from "../store/global";


const Home = lazy(() => import('./Home'))
const NotFound = lazy(() => import('./NotFound'))
const UserPage = lazy(() => import('./UserPage'))
const SignIn = lazy(() => import('./SignIn'))
const SignUp = lazy(() => import('./SignUp'))
const AdminDashboard = lazy(() => import('./AdminDashboard'))

const openPages = () =>{
    const pages = new Map()
    pages.set("", <Home />)
    pages.set("/home", <Navigate to="" replace />)
    pages.set("*", <Home />)
    return pages
}

const pageWithoutAuth = () => {
    const pages = openPages()
    pages.set("/signIn", <SignIn/>)
    pages.set("/signUp", <SignUp/>)
    return pages
}
const pageWithAuth = () => {
    const pages = openPages()
    pages.set("/profile", <UserPage />)
    pages.set("/admin", <AdminDashboard />)
    return pages
}
const createRoutes = isAuth => {
    const pages = isAuth ? pageWithAuth() : pageWithoutAuth();
    return [...pages].reduce((acc, [path, component]) => {
        acc.push({ path, component })
        return acc
    }, [])
}
export function AppRoutes(props) {
    const {isAuth} = props
    const routes = useMemo(() => createRoutes(isAuth), [isAuth])
    return (
        <Suspense fallback={<Loader />}>
            <Routes>
                {
                    routes.map(({ path, component }) => {
                        return (
                            <Route key={path} path={path} element={component} />
                        )
                    })
                }
            </Routes>
        </Suspense>
    )
};
const mapStateTopProps = (state) => ({
    isAuth: state.global.isAuth,
});
const mapDispatchToProps = (state) => ({
});
export default connect(mapStateTopProps, mapDispatchToProps)(AppRoutes);