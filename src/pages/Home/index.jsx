import * as React from 'react';
import AppBar from '@mui/material/AppBar';
import Button from '@mui/material/Button';
import CameraIcon from '@mui/icons-material/PhotoCamera';
import Card from '@mui/material/Card';
import CardActions from '@mui/material/CardActions';
import CardContent from '@mui/material/CardContent';
import CssBaseline from '@mui/material/CssBaseline';
import Grid from '@mui/material/Grid';
import Box from '@mui/material/Box';
import Toolbar from '@mui/material/Toolbar';
import Typography from '@mui/material/Typography';
import Container from '@mui/material/Container';
import Link from '@mui/material/Link';
import Divider from '@mui/material/Divider';
import { createTheme, ThemeProvider } from '@mui/material/styles';
import {connect} from "react-redux";
import {setAuth} from "../../store/global/globalSlice";
import {setUserData} from "../../store/user/userSlice";
import UserInfoPopover from "../../components/UserInfoPopover"
import ProductsFormPopup from "../../components/ProductsFormPopup"
import {setProducts,setProductIndication} from "../../store/product/productSlice";
import {useEffect} from "react";
function Copyright() {
    return (
        <Typography variant="body2" color="text.secondary" align="center">
            {'Copyright © '}
            <Link color="inherit" href="https://mui.com/">
                Your Website
            </Link>{' '}
            {new Date().getFullYear()}
            {'.'}
        </Typography>
    );
}

const theme = createTheme();

const Home = (props) => {
    const {isAuth, userData, products, productCount, setProducts, setProductIndication, productIndication} = props

    const [open, setOpen] = React.useState(false);

    const [showProducts, setShowProducts] = React.useState(false);

    const handleAddProduct = (result) => {
        setProducts(result)
    }
    const handleFilterProducts =  React.useCallback (() =>{
        let result = []
        let locProductIndication = [...productIndication]
        function GetRandomLikesObject (arr = []) {
            const objecsCount = arr.length
            const objects = arr.filter (el => !locProductIndication.includes(el.id))
            /* Sort array as bigger to low index */
            var sortedObjects = [...objects].sort((next, prev) => next.weight > prev.weight ? -1 : 1);
            /* get random number don't more then length of sorted array */
            var randomIndexesArray = Array.from({ length: objecsCount }, () => Math.round(objecsCount * Math.random()));
            /* Get lower index */
            var lowestIndex = Math.min(...randomIndexesArray);
            if(sortedObjects[lowestIndex]){
                locProductIndication = [sortedObjects[lowestIndex].id,...locProductIndication]
            }else{
                locProductIndication = []
                result = []
                return  GetRandomLikesObject(arr)
            }
           return sortedObjects[lowestIndex]
        }
        const cycleCount = products.length <=5 ? products.length : 5
        while (result.length < cycleCount){
            const randItem = GetRandomLikesObject(products)
            result.push(randItem)
        }
        setProductIndication(locProductIndication)
        return result
    },[products])

    useEffect(()=>{
        setShowProducts(()=>handleFilterProducts())
    },[products])
    return (
        <ThemeProvider theme={theme}>
            <CssBaseline />
            <AppBar position="relative">
                <Toolbar>
                    <CameraIcon sx={{ mr: 2 }} />
                    <Typography variant="h6" color="inherit" noWrap component="div" sx={{ flexGrow: 1 }}>
                        Album layout
                    </Typography>
                    {isAuth
                        ?<>
                            <UserInfoPopover>{userData.email}</UserInfoPopover>
                        </>
                        :<>
                            <Link href="/signIn" color="inherit" variant="body2">
                                Sign In
                            </Link>
                            <span>/</span>
                            <Link href="/signIn" color="inherit" variant="body2">
                                Sign Up
                            </Link>
                        </>
                    }

                </Toolbar>
            </AppBar>
            <main>
                {/* Hero unit */}
                <Box
                    sx={{
                        bgcolor: 'background.paper',
                        pt: 8,
                        pb: 6,
                    }}
                >
                    <Container maxWidth="sm">
                        <Typography
                            component="h1"
                            variant="h2"
                            align="center"
                            color="text.primary"
                            gutterBottom
                        >
                            Album
                        </Typography>
                        {
                            isAuth
                            ?<ProductsFormPopup handleAddProduct={handleAddProduct}/>
                            :(
                                    <Typography
                                        component="h1"
                                        variant="h3"
                                        textAlign="center"
                                        color="text.primary"
                                    >
                                        Pleas sign in/up to your account
                                    </Typography>
                            )
                        }

                    </Container>
                </Box>
                <Container sx={{ py: 8 }} maxWidth="md">
                    {/* End hero unit */}
                    <Grid container spacing={3} align="center">
                        {showProducts.length > 0
                           ? showProducts.map((card) => (
                            <Grid item key={card} xs={12} sm={6} md={4}>
                                <Card
                                    sx={{ height: '100%', display: 'flex', flexDirection: 'column' }}
                                >
                                    <CardContent sx={{ flexGrow: 1 }}>
                                        <Typography gutterBottom variant="h5" component="h2">
                                            {card.name}
                                        </Typography>
                                        <Typography>
                                            {card.weight} $
                                        </Typography>
                                        <Typography>
                                            {card.weight} KG
                                        </Typography>
                                        <Divider light />
                                        <Typography component={'h6'} variant="h6" textAlign={'right'}>
                                            {card.dateStart}
                                        </Typography>
                                        <Typography component={'h6'} variant="h6" textAlign={'right'}>
                                            {card.dateEnd}
                                        </Typography>
                                    </CardContent>
                                </Card>
                            </Grid>
                            ))
                            :(
                                <Grid container spacing={3} align="center" justifyContent={'center'}>
                                    <Typography
                                        component="h1"
                                        variant="h3"
                                        textAlign="center"

                                        color="text.primary"
                                    >
                                        Pleas add products
                                    </Typography>
                                </Grid>
                            )
                        }
                    </Grid>
                </Container>
            </main>
            {/* Footer */}
            <Box sx={{ bgcolor: 'background.paper', p: 6 }} component="footer">
                <Typography variant="h6" align="center" gutterBottom>
                    Footer
                </Typography>
                <Typography
                    variant="subtitle1"
                    align="center"
                    color="text.secondary"
                    component="p"
                >
                    Something here to give the footer a purpose!
                </Typography>
                <Copyright />
            </Box>
            {/* End footer */}
        </ThemeProvider>
    );
}

const mapStateTopProps = (state) => ({
    isAuth: state.global.isAuth,
    userData:state.user.userData,
    products: state.product.products,
    productCount: state.product.productCount,
    productIndication: state.product.productIndication

})
const mapDispatchToProps = {
    setAuth:setAuth,
    setUser:setUserData,
    setProducts:setProducts,
    setProductIndication:setProductIndication,

}

export default connect(mapStateTopProps, mapDispatchToProps)(Home);