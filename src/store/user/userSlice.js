import { createSlice } from "@reduxjs/toolkit";
import { initialUserState } from "./initialState";

export const userSlice = createSlice({
    name: "user",
    initialState: initialUserState,
    reducers: {
        setUserData(state, action) {
            state.userData = action.payload;
        },
        setUserDbData(state, action) {
            state.userDbData = [
                action.payload,
                ...state.userDbData
            ];
        },
    },
});
export const  {setUserData, setUserDbData} = userSlice.actions
export default userSlice.reducer