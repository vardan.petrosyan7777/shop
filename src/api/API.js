import axios from "axios"
const API = axios.create({
    baseURL: `${process.env.REACT_APP_API}/api/`,
    withCredentials: true
})

axios.interceptors.request.use(
    async (config) => {
        const token = await localStorage.getItem("accessToken")
        if (token) {
            config.headers["accessToken"] = `Bearer ${token}`
            config.headers["Authorization"] = `Bearer ${token}`
        }
        return config
    },
    (error) => {
        return Promise.reject(error)
    }
)
axios.interceptors.response.use(
    (response) => {
        return response
    },
    (err) => {
        return new Promise(async (resolve, reject) => {
            if (err?.response?.status === 401) {
                const token = localStorage.getItem("accessToken")
                const refreshToken = localStorage.getItem("refreshToken")
                const headers = {
                    accessToken: token,
                    refreshToken: refreshToken,
                }
                await axios
                    .put(
                        `${process.env.REACT_APP_API}/api/User/RefreshAccessToken`,
                        {
                            headers: headers,
                        }
                    )
                    .then(async (response) => {
                        if (response.data.accepted === true) {
                            await localStorage.setItem(
                                "accessToken",
                                response.data.data[0].accessToken
                            )
                            await localStorage.setItem(
                                "refreshToken",
                                response.data.data[0].refreshToken
                            )
                            err.config.headers[
                                "accessToken"
                                ] = `Bearer ${response.data.data[0].accessToken}`
                            err.config.headers[
                                "Authorization"
                                ] = `Bearer ${response.data.data[0].accessToken}`
                            return axios(err.config)
                        } else {
                            await localStorage.removeItem("accessToken")
                            await localStorage.removeItem("refreshToken")
                            return Promise.reject(err)
                        }
                    })
                return axios(err.config)
            } else {
                return Promise.reject(err)
            }
        })
    }
)
export default API;
export { axios };