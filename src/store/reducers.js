import { applyMiddleware, combineReducers, createStore } from "redux";
import {configureStore} from "@reduxjs/toolkit";
import { persistStore,
        persistReducer,
        FLUSH,
        REHYDRATE,
        PAUSE,
        PERSIST,
        PURGE,
        REGISTER,
} from 'redux-persist'
import storage from 'redux-persist/lib/storage' // defaults to localStorage for web
import { composeWithDevTools } from "redux-devtools-extension";
import thunk from "redux-thunk";
import globalReducer from "./global/globalSlice";
import userReducer from "./user/userSlice";
import productReducer from "./product/productSlice";

const persistConfig = {
    key: 'root',
    storage,
}
const rootReducer = combineReducers({
    global: globalReducer,
    user: userReducer,
    product: productReducer
});
const persistedReducer = persistReducer(persistConfig,rootReducer)

const store  = configureStore({
    reducer:persistedReducer,
    middleware: (getDefaultMiddleware) =>
        getDefaultMiddleware({
            serializableCheck: {
                ignoredActions: [FLUSH, REHYDRATE, PAUSE, PERSIST, PURGE, REGISTER],
            },
        }
    ),
});
export const persistor = persistStore(store)
export default store
// composeWithDevTools(applyMiddleware(thunk))