import Portal from "../../portal";
const stylePopup = {
    position:'fixed',
    top:0,
    left:0,
    right:0,
    bottom:0,
    zIndex:1000,
    display: 'grid',
    placeItems:'center'
}
const styleOverlay = {
    position:'absolute',
    width:'100%',
    height:'100%',
    backgroundColor:'#1B283A',
    zIndex: 1
}
const styleContent = {
    zIndex: 2,
    maxWidth: 'max-content',
    maxHeight: 'max-content'
}
const Popup = ({children,onClose,isOpened}) => {
    if(!isOpened){
        return null
    }
    return  <Portal>
                <div className={'popup'} style={stylePopup} role={'dialog'}>
                    <div className={'overlay'}
                         style={styleOverlay}
                         role={'button'}
                         tabIndex={0}
                         onClick={onClose}/>
                    <div className={'content'} style={styleContent}>
                        {children}
                    </div>
                </div>
            </Portal>
}
export default Popup